# 01 Metaúdaje

Diskusný priestor pre problematiku podpory tvorby, správy a využívania metaúdajov. Jednotlivé príspevky je možné vytvárať [v časti Issues](https://gitlab.com/mzpsr/mzpsr/podpora-inspire-implement-cie/01-meta-daje/issues).
Dokumentácia príkladov dobrej praxe a výsledkov diskusií je k dispozícii na [Wiki Dobrej praxe](https://gitlab.com/mzpsr/mzpsr/podpora-inspire-implement-cie/01-meta-daje/wikis/Dobr%C3%A1-prax-pre-Meta%C3%BAdaje).